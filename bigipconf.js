const fs = require('fs');

fs.readFile('./example.conf', 'utf8', (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  
  const nearley = require("nearley");
  const grammar = require("./grammar.js");

  // Create a Parser object from our grammar.
  const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar));

  // Parse something!
  parser.feed(data);

  // parser.results is an array of possible parsings.
  console.log(JSON.stringify(parser.results)); 
});
