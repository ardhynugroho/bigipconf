bigip_conf -> block_vs:+

block_vs  
  -> 
    "ltm virtual" obj _ "{"
    description:?
    destination:?
    ip_protocol:?
	  mask:?
	  pool:?
	  profiles:?
    rules:?
    source:?
    snat:?
    translate_addr:?
    translate_port:?
	  vlans:?
    vlans_enabled:?
	  _ "}"

description -> _ "description" [\w\"\.\ ]:+

destination -> _ "destination" obj

ip_protocol -> _ "ip-protocol" _ [\w]:+

mask -> _ "mask" _ [\w\.]:+

pool -> _ "pool" obj

profiles -> _ "profiles" _ "{" objs_p _ "}"

rules -> _ "rules" _ "{"  objs _ "}"

source -> _ "source" _ [0-9./]:+

snat -> _ "source-address-translation" _ "{" pool _ "type" _ [\w]:+ _ "}"

translate_addr -> _ "translate-address" _ [\w]:+

translate_port -> _ "translate-port" _ [\w]:+

vlans -> _ "vlans" _ "{" objs _ "}" #{% function(d) {return d[1] + d[5] } %}

vlans_enabled -> _ "vlans-enabled"

# multiple config object with parameter
objs_p
  -> obj_p {% id %}
  |  objs_p obj_p {% function(d) {return d[0] + "," + d[1]} %}

# configuration object with parameter
# example: 
#   /Common/cs_ssl {
#     context clientside
#   }
obj_p 
  -> obj _ "{" _ "}" {% id %}
  |  obj _ "{" params _ "}" {% id %}

# multiple configuration object
objs
  -> obj {% id %}
  |  objs obj {% function(d) {return d[0]+","+d[1]} %}

# configuration object reference
obj 
  -> _ "/" [\w]:+ "/" [\w\.\:]:+ {% function (d) {return d[1]+d[2].join("")+d[3]+d[4].join("")} %}

# match: multiple parameters
params 
  -> param
  | params param

# general config statement
# example: mask 255.255.255.0  
param 
  -> _ [\w]:+ " " [\w\.]:+

# white space / tab / newline
_ -> [\s\n\t]:+ {% function(d) {return null} %}